Plugin.require_version("1.13")

selector = config["selector"]

elems = HTML.select(page, selector)
local count = size(elems)
local i = 1
while (i <= count) do
  style = HTML.get_attribute(elems[i], "style")
  if style then
    HTML.delete_attribute(elems[i], "style")
  end
  i = i + 1
end
