# site

Source code for [my website](https://liassica.codeberg.page), hosted on [Codeberg Pages](https://codeberg.page/)

## Contact

If you have inquiries, suggestions, or bug reports, you can [file an issue](https://codeberg.org/Liassica/site/issues/new) or message me on <a rel="me" href="https://tech.lgbt/@Liassica">Mastodon</a>.

## License

See [LICENSE](./LICENSE). As a general rule, source code of this site is licensed under the [GNU Affero General Public License 3.0 or later](https://www.gnu.org/licenses/agpl-3.0) and content of the site is licensed under either [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) or [Attribution-NoDerivs 4.0 International](https://creativecommons.org/licenses/by-nd/4.0/).
