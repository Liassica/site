[settings]
  # Soupault version that the config was written/generated for
  # Trying to process this config with an older version will result in an error message
  soupault_version = "4.7.0"

  # Stop on page processing errors?
  strict = true

  # Display progress?
  verbose = true

  # Display detailed debug output?
  debug = false

  # Where input files (pages and assets) are stored.
  site_dir = "src"

  # Where the output goes
  build_dir = "public"

  # Files inside the site/ directory can be treated as pages or static assets,
  # depending on the extension.
  #
  # Files with extensions from this list are considered pages and processed.
  # All other files are copied to build/ unchanged.
  #
  # Note that for formats other than HTML, you need to specify an external program
  # for converting them to HTML (see below).
  page_file_extensions = ["htm", "html", "md", "rst", "adoc"]

  # By default, soupault uses "clean URLs",
  # that is, $site_dir/page.html is converted to $build_dir/page/index.html
  # You can make it produce $build_dir/page.tml instead by changing this option to false
  clean_urls = true

  # If you set clean_urls=false,
  # file names with ".html" and ".htm" extensions are left unchanged.
  keep_extensions = ["html", "htm"]

  # All other extensions (".md", ".rst"...) are replaced, by default with ".html"
  default_extension = "html"

  # Page files with these extensions are ignored.
  ignore_extensions = ["draft"]

  # Soupault can work as a website generator or an HTML processor.
  #
  # In the "website generator" mode, it considers files in site/ page bodies
  # and inserts them into the empty page template stored in templates/main.html
  #
  # Setting this option to false switches it to the "HTML processor" mode
  # when it considers every file in site/ a complete page and only runs it through widgets/plugins.
  generator_mode = true

  # Files that contain an <html> element are considered complete pages rather than page bodies,
  # even in the "website generator" mode.
  # This allows you to use a unique layout for some pages and still have them processed by widgets.
  complete_page_selector = "html"

  # Website generator mode requires a page template (an empty page to insert a page body into).
  # If you use "generator_mode = false", this file is not required.
  default_template_file = "templates/main.html"

  # Page content is inserted into a certain element of the page template.
  # This option is a CSS selector that is used for locating that element.
  # By default the content is inserted into the <body>
  default_content_selector = "main"

  # You can choose where exactly to insert the content in its parent element.
  # The default is append_child, but there are more, including prepend_child and replace_content
  default_content_action = "append_child"

  # If a page already has a document type declaration, keep the declaration
  keep_doctype = true

  # If a page does not have a document type declaration, force it to HTML5
  # With keep_doctype=false, soupault will replace existing declarations with it too
  doctype = "<!DOCTYPE html>"

  # Insert whitespace into HTML for better readability
  # When set to false, the original whitespace (if any) will be preserved as is
  pretty_print_html = true

  # Plugins can be either automatically discovered or loaded explicitly.
  # By default discovery is enabled and the place where soupault is looking is the plugins/ subdirectory
  # in your project.
  # E.g., a file at plugins/my-plugin.lua will be registered as a widget named "my-plugin".
  plugin_discovery = true
  plugin_dirs = ["plugins"]

  # Soupault can cache outputs of external programs
  # (page preprocessors and preprocess_element widget commands).
  # It's disabled by default but you can enable it and configure the cache directory name/path
  caching = true
  cache_dir = ".cache/soupault"

  # Soupault supports a variety of page source character encodings,
  # the default encoding is UTF-8
  page_character_encoding = "utf-8"
 

# It is possible to store pages in any format if you have a program
# that converts it to HTML and writes it to standard output.
# Example:
[preprocessors]
  md = "echo 'pandoc --from=markdown --to=html $1 | prettier --parser=html' | sh -s"

[asset_processors]
  scss = "if [[ ! {{source_file_name}} =~ ^[_] ]]; then sass {{source_file_path}} {{target_dir}}/{{source_file_base_name}}.css | prettier --parser=css; fi"

# Pages can be further processed with "widgets"

# Takes the content of the first <h1> and inserts it into the <title>
[widgets.page-title]
  widget = "title"
  selector = ["#page-title", "h1"]
  default = "Liassica's site"
  append = " - Liassica's site"

  # Insert a <title> in a page if it doesn't have one already.
  # By default soupault assumes if it's missing, you don't want it.
  force = false

# Inserts a generator meta tag in the page <head>
# Just for demonstration, feel free to remove
[widgets.generator-meta]
  widget = "insert_html"
  html = '<meta name="generator" content="soupault">'
  selector = "head"

[widgets.insert-favicon]
  widget = "insert_html"
  html = '<link rel="icon" type="image/png" href="/images/favicon.webp">'
  selector = "head"

[widgets.safe-links]
  widget = "safe-links"
  attributes = ["nofollow", "noopener", "noreferrer"]
  targets = ["_blank"]

[widgets.highlight-active-link]
  widget = "section-link-highlight"
  selector = "nav"
  active_link_class = "nav-active"

[widgets.insert-license-cc-by-sa]
  widget = "insert-if"
  html = 'Copyright &copy; 2024 Liassica. Except where noted, the content of this page is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="license noopener noreferrer">CC BY-SA 4.0</a>.'
  selector = "footer"
  check_selector = ".cc-by-sa"
  after = "make-post-header"

[widgets.insert-license-cc-by-nd]
  widget = "insert-if"
  html = 'Copyright &copy; 2024 Liassica. Except where noted, the content of this page is licensed under <a href="https://creativecommons.org/licenses/by-nd/4.0/" target="_blank" rel="license noopener noreferrer">CC BY-ND 4.0</a>.'
  selector = "footer"
  check_selector = ".cc-by-nd"
  after = "make-post-header"

[widgets.remove-inline-table-styling]
  widget = "remove-inline-styling"
  selector = "table"

[widgets.remove-inline-col-styling]
  widget = "remove-inline-styling"
  selector = "table col"

[widgets.make-post-header]
  widget = "post-header"
  section = "posts/"
  exclude_page = "posts/index.md"

  content_container_selector = "main"

  post_header_template = '''
    <div id="post-header" class="{{license}}">
      <h1 id="post-title">{{title}}</h1>
      <div><strong>Posted:</strong> <time id="post-date" datetime="{{post_date}}">{{post_date}}</time></div>
      {% if update_date %}
      <div><strong>Updated:</strong> <time id="update-date" datetime="{{update_date}}">{{update_date}}</time></div>
      {% endif %}
      {% if tags %}
        <div class="post-tags">
          <strong>Tags: </strong>
          {%- for t in tags -%}
            <a href="/posts/tag/{{t}}"><span class="post-tag">{{t}}</span></a>{% if not loop.last %}, {% endif %}
          {%- endfor -%}
        </div>
      {% endif %}
    </div>
  '''

[index.fields]
  body = { selector = ["main"] }
  excerpt = { selector = ["#post-excerpt", "p"] }
  post_date = { selector = ["#post-date", "time"], extract_attribute = "datetime", fallback_to_content = true }
  update_date = { selector = ["#update-date", "time"], extract_attribute = "datetime", fallback_to_content = true }
  tags = { selector = ".post-tag", select_all = true }
  title = { selector = [ "#post-title", "h1"] }

[index]
  index = true

  sort_descending = true
  sort_type = "calendar"
  sort_by = "post_date"

  extract_after_widgets = ['make-post-header']

  date_formats = ["%F"]

[index.views.posts]
  index_selector = "#posts-index"
  section = "posts"
  index_template = """
    {% for e in entries %}
    <h2><a href="{{e.url}}">{{e.title}}</a></h2>
    <div class="post-metadata">
      <div><strong>Posted:</strong> <time id="post-date" datetime="{{e.post_date}}">{{e.post_date}}</time></div>
      {% if e.update_date %}
      <div><strong>Updated:</strong> <time id="update-date" datetime="{{e.update_date}}">{{e.update_date}}</time></div>
      {% endif %}
      {% if e.tags %}
      <div class="post-tags">
        <strong>Tags: </strong>
        {%- for t in e.tags -%}
          <a href="/posts/tag/{{t}}"><span class="post-tag">{{t}}</span></a>{% if not loop.last %}, {% endif %}
        {%- endfor -%}
      </div>
      {% endif %}
    </div>
    <p>{{e.excerpt}}</p>
    <a href="{{e.url}}">Read more</a>
    {% endfor %}
  """

  file = "helpers/posts-index.lua"

[custom_options]
  ## Atom feed settings
  atom_feeds = true

  # If you want to generate Atom feeds, you will need to adjust the site metadata config below:

  # Required:
  #site_url = "http://localhost:8080" # Testing
  site_url = "https://liassica.codeberg.page" # Real

  # Optional but strongly recommended:
  site_author = "Liassica"
  # site_author_email = "jrandomhacker@example.com"
  site_title = "Liassica's site"
  site_logo = "https://liassica.codeberg.page/images/favicon.png"

  # Completely optional:
  # site_subtitle = "Some subtitle"

[widgets.atom]
  widget = "atom"
  page = "posts/index.md"
  feed_file = "atom.xml"
  use_section = "posts"
