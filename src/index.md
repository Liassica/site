# Welcome to my site! {.cc-by-nd}

## Info

Hello, my name is Liassica, and welcome to my site.
Here you can find: my [posts](/posts), which include guides, music reviews (coming soon), and whatever else I feel like talking about; [links](/links) to other sites I use; and the famed [Grszh Gallery](/grszh).

My old site was found on [Neocities](https://liassica.neocities.org/) and [GitHub Pages](https://liassica.github.io/), but they redirect here now.

## Contact

If you have inquiries, suggestions, or bug reports, you can [file an issue](https://codeberg.org/Liassica/site/issues/new) on the [Codeberg repository for this site](https://codeberg.org/Liassica/site).

Otherwise, you can also find me here:

- [Mastodon (\@Liassica\@tech.lgbt)](https://tech.lgbt/@Liassica){rel="me"}
- [Pronouns.page (\@Liassica)](https://en.pronouns.page/@Liassica){rel="me"}

## Webrings

### This site is part of the:

::::: {.webrings}
:::: {.garfring}
::: {.garfring--controls}
[<-- prev](https://r-temisia.neocities.org/)
:::
::: {#garfring--index}
[Garfield webring](https://garfring.neocities.org/)
:::
::: {.garfring--controls}
[next -->](https://anti-kythera.neocities.org/)
:::
::::
:::::

## Buttons

### Me and this site

Add my button to your site:

```html
<a href="https://liassica.codeberg.page/">
  <img
    src="https://liassica.codeberg.page/images/buttons/liassica.webp"
    alt="Liassica!"
  />
</a>
```

::: {.buttons}
[![Liassica!](/images/buttons/liassica.webp)](https://liassica.codeberg.page/)
[![Follow me on Mastodon](/images/buttons/mastodon.webp)](https://tech.lgbt/@Liassica){rel="me"}
[![Pronouns.page](/images/buttons/pronouns-page.webp)](https://en.pronouns.page/@Liassica){rel="me"}
[![Powered by Soupalt](/images/buttons/soupault.webp)](https://soupault.app/)
:::

### People I know

::: {.buttons}
[![The JoxiZone](/images/buttons/joxizone.webp)](https://joxi.neocities.org/)
[![Artie, Enby Scene Queen](/images/buttons/artie.webp)](https://r-temisia.neocities.org/)
[![April :3](/images/buttons/april.webp)](https://gingeh.neocities.org/)
:::

### The wonderful creator of my favorite game series ([_Lonely Wolf Treat_](https://nomnomnami.itch.io/treat-complete))

::: {.buttons}
[![NomnomNami](/images/buttons/nomnomnami.webp)](https://nomnomnami.com/)
:::
