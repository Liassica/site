# Grszh Gallery credits & fair use disclaimer {.cc-by-nd}

## Credits

[Original "Grszh" image](https://twitter.com/takeru0321neko/status/1459823058751922179) by [\@takeru0321neko on Twitter](https://twitter.com/takeru0321neko).
See [fair use disclaimer](#fair-use-disclaimer).

Edited "Grszh" images by [Aleph](https://alephs.neocities.org/), [Artie](https://r-temisia.neocities.org/), Cattum, Dalek, [Elot](https://elot.neocities.org/), Kemet, Luna, [Pluck](https://pluckyporo.carrd.co/), Shalodey, [Sylvia](https://4mpress.neocities.org/), myself (Liassica), and other friends.

## Fair use disclaimer

Section 107 of the Copyright Act of 1976 allows fair use of a copyrighted work for purposes such as criticism, comment, news reporting, teaching, scholarship, or research.
Neither the creator(s) nor the host(s) of this site benefit financially from displaying copyrighted works.
The full "Grszh" image is displayed as it is not replaceable by free content and is needed to identify the original work that has been edited by contributors to this site.
It and the edited versions are displayed in low quality as to allow identification while remaining inferior to the original image.
Edited versions of the original image are believed to be transformative enough that, along with the above conditions, they qualify as fair use.
