# Grszh Gallery submission information {.cc-by-nd}

If you would like to contribute to the Grszh Gallery, send me an edited version of the [original "Grszh" image](https://twitter.com/takeru0321neko/status/1459823058751922179) in an aspect ratio of 3:4.
