# Posts {.cc-by-nd}

Things I've written to go on this site. Includes guides, opinions, and whatever else I feel like. [Atom feed for news aggregators/RSS readers](atom.xml).

::: {#posts-index}

<!-- posts index will be inserted here -->

:::
