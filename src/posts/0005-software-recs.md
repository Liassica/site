<post-metadata>
  <post-title>Software recommendations</post-title>
  <post-date>2024-08-10</post-date>
  <update-date>2024-08-18</update-date>
  <post-tags>browsers, linux, opinion</post-tags>
  <post-license>cc-by-nd</post-license>
</post-metadata>

## Overview

<post-excerpt>This post lists software I recommend for various use cases.</post-excerpt>
Idea blatantly stolen from [geck](https://geckro.github.io/html/lists/software.html).

<!-- prettier-ignore-start -->

Table: Operating systems

+---------+-----------------------+----------------------------------------------------------------+
| OS      | OS                    | Explanation                                                    |
| family  |                       |                                                                |
+=========+=======================+================================================================+
| Linux   | [NixOS]               | Currently my main operating system. Its main features are      |
|         |                       | declarative system configuration and generational deployment.  |
|         |                       | This not only makes it extremely robust, but it also makes it  |
|         |                       | very easy to copy configurations between devices or restore    |
|         |                       | them after a reinstallation. You can find                      |
|         |                       | [my NixOS configuration] on my Codeberg.                       |
|         +-----------------------+----------------------------------------------------------------+
|         | [Secureblue]          | Hardened Fedora Atomic images. One of the best choices for a   |
|         |                       | (relatively) secure Linux desktop and built upon a robust      |
|         |                       | Fedora Atomic base.                                            |
|         +-----------------------+----------------------------------------------------------------+
|         | [Arch Linux]          | Flexible DIY distro. While I generally prefer NixOS, I've      |
|         |                       | enough experience with Arch to recommend it to those who don't |
|         |                       | want to learn NixOS's ins and outs.                            |
|         +-----------------------+----------------------------------------------------------------+
|         | [GrapheneOS]          | Hardened Android derivative. The best option for mobile        |
|         |                       | devices.                                                       |
+---------+-----------------------+----------------------------------------------------------------+
| Windows | [Windows 11 IoT LTSC] | The only somewhat usable version of Windows. I avoid using     |
|         |                       | Windows when I can because I don't particularly like it. Good  |
|         |                       | luck trying to get your hands on a copy, though; the education |
|         |                       | edition is probably good enough if you can't.                  |
+---------+-----------------------+----------------------------------------------------------------+

<!-- prettier-ignore-end -->

[NixOS]: https://nixos.org
[my NixOS configuration]: https://codeberg.org/Liassica/nixos-config
[Secureblue]: https://github.com/secureblue/secureblue
[Arch Linux]: https://archlinux.org/
[GrapheneOS]: https://grapheneos.org/
[Windows 11 IoT LTSC]: https://microsoft.com/evalcenter/evaluate-windows-11-iot-enterprise-ltsc

<!-- prettier-ignore-start -->

Table: Web browsers

+------------+---------------------+---------------------------------------------------------------+
| OS         | Browser             | Explanation                                                   |
+============+=====================+===============================================================+
| Fedora     | [Hardened Chromium] | The name is self-explanatory. Uses a set of custom patches as |
|            |                     | well as patches from GrapheneOS's Vanadium to reduce the      |
|            |                     | attack surface and increase the security of the standard      |
|            |                     | Fedora Chromium package.                                      |
+------------+---------------------+---------------------------------------------------------------+
| Other      | [Chromium Flatpak]  | Most other distros do not compile Chromium correctly by       |
| desktop    |                     | introducing lots of downstream patches that weaken security,  |
| Linux      |                     | compiling debug builds instead of production builds, or       |
|            |                     | dynamically linking Chromium to unhardened system libraries   |
|            |                     | and thereby [severely weakening control flow integrity]       |
|            |                     | (CFI). The Chromium Flatpak mitigates some of this by         |
|            |                     | replacing Chromium's built-in sandboxing with Flatpak's       |
|            |                     | sandboxing.                                                   |
+------------+---------------------+---------------------------------------------------------------+
| GrapheneOS | [Vanadium]          | Hardened Chromium-based browser for GrapheneOS.               |
+------------+---------------------+---------------------------------------------------------------+
| Other      | [Cromite]           | Chromium-based Android browser with some privacy              |
| Android    |                     | enhancements.                                                 |
+------------+---------------------+---------------------------------------------------------------+
| Windows    | [Microsoft Edge]    | Edge's strict enhanced security mode disables just-in-time    |
|            |                     | (JIT) compilation for all sites,                              |
|            |                     | [massively reducing the attack vector of V8], Chromium's      |
|            |                     | JavaScript engine. However, the addition of the [DrumBrake]   |
|            |                     | WebAssembly (WASM) interpreter allows you to still use WASM   |
|            |                     | with enhanced security enabled. This mode also enables use of |
|            |                     | advanced Windows security features such as                    |
|            |                     | [Control Flow Guard] and [Arbitrary Code Guard]. Finally, by  |
|            |                     | using Edge, you avoid trusting an extra party by installing a |
|            |                     | different browser, as you're already trusting Microsoft by    |
|            |                     | using Windows.                                                |
|            +---------------------+---------------------------------------------------------------+
|            | [Mozilla Firefox]   | If you *really* don't like the idea of using Edge, the most   |
|            |                     | secure version of Firefox is the [Windows one]. Make sure to  |
|            |                     | check out my [Arkenfox guide] to get the most out of Firefox! |
+------------+---------------------+---------------------------------------------------------------+

<!-- prettier-ignore-end -->

Generally avoid Firefox Mobile and its derivatives as it is missing important security features [such as site isolation].

With the exception of Edge on Windows, generally avoid proprietary Chromium-based desktop browsers such as Opera or Vivaldi as they generally provide nothing security-wise over Chromium/Edge while adding attack surface, extra parties you're required to trust, and tracking.

[Hardened Chromium]: https://github.com/secureblue/hardened-chromium
[Chromium Flatpak]: https://flathub.org/apps/org.chromium.Chromium
[severely weakening control flow integrity]: https://clang.llvm.org/docs/ControlFlowIntegrity.html#:~:text=To%20allow%20the%20checks%20to%20be%20implemented%20efficiently%2C%20the%20program%20must%20be%20structured%20such%20that%20certain%20object%20files%20are%20compiled%20with%20CFI%20enabled%2C%20and%20are%20statically%20linked%20into%20the%20program.
[Vanadium]: https://github.com/GrapheneOS/Vanadium
[Cromite]: https://github.com/uazo/cromite
[Microsoft Edge]: https://microsoft.com/edge
[massively reducing the attack vector of V8]: https://microsoftedge.github.io/edgevr/posts/Super-Duper-Secure-Mode/#:~:text=it%20would%20remove%20roughly%20half%20of%20the%20V8%20bugs%20that%20must%20be%20fixed
[DrumBrake]: https://microsoftedge.github.io/edgevr/posts/Introducing-Enhanced-Security-for-Microsoft-Edge/#introducing-drum-brake-a-webassembly-interpreter
[Control Flow Guard]: https://learn.microsoft.com/windows/win32/secbp/control-flow-guard
[arbitrary code guard]: https://learn.microsoft.com/defender-endpoint/exploit-protection-reference#arbitrary-code-guard
[Mozilla Firefox]: https://mozilla.org/firefox
[Windows one]: https://madaidans-insecurities.github.io/firefox-chromium.html#windows-sandbox
[Arkenfox guide]: /posts/0000-arkenfox
[such as site isolation]: https://bugzilla.mozilla.org/show_bug.cgi?id=1610822

<!-- prettier-ignore-start -->

Table: Browser extensions

+----------------------+---------------------------------------------------------------------------+
| Extension            | Explanation                                                               |
+======================+===========================================================================+
| [uBlock Origin Lite] | Efficient general-purpose content blocker. Stick to its default filter    |
+----------------------+ lists to avoid standing out and prevent the possibility of a malicious    |
| [uBlock Origin]      | filter rule being added; prefer Lite on Chromium-based browsers so that   |
|                      | you can completely avoid Manifest V2, [improving security].               |
+----------------------+---------------------------------------------------------------------------+

<!-- prettier-ignore-end -->

First rule of browser extensions: less is more. Install as few extensions as possible; the more you have installed, the greater your attack surface and the more you stand out. I personally only have a single extension, [Bitwarden password manager], and if Bitwarden Desktop for Linux ever gains the ability to handle passkey requests from the browser I may consider ditching even that. I use [AdGuard's public DNS servers] to provide ad blocking so that I don't need to install uBlock Origin.

[uBlock Origin Lite]: https://ublockorigin.com/
[uBlock Origin]: https://ublockorigin.com/
[improving security]: https://developer.chrome.com/docs/extensions/develop/migrate/improve-security
[Bitwarden password manager]: https://chromewebstore.google.com/detail/bitwarden-password-manage/nngceckbapebfimnlniiiahkandclblb
[AdGuard's public DNS servers]: https://adguard-dns.io/en/public-dns.html

<!-- prettier-ignore-start -->

Table: Desktop applications

+----------+--------------+------------------------------------------------------------------------+
| Category | App          | Explanation                                                            |
+==========+==============+========================================================================+
| Text     | [Neovim]     | Modern Vim fork with native Lua support. Fast and simple. I personally |
| editor/  |              | use a [LazyVim]-based configuration.                                   |
| IDE      +--------------+------------------------------------------------------------------------+
|          | [Emacs]      | Extensible GUI and TUI text editor. Emacs can do anything.             |
|          |              | personally use a [Doom Emacs]-based configuration.                     |
|          +--------------+------------------------------------------------------------------------+
|          | [VSCodium]   | Builds of Code OSS with Microsoft telemetry removed. The most          |
|          |              | beginner-friendly of these options.                                    |
+----------+--------------+------------------------------------------------------------------------+
| Notes    | [Org mode]   | Major mode and plain-text file format for Emacs. Similar to Markdown   |
|          |              | in many ways but a bit more flexible.                                  |
|          +--------------+------------------------------------------------------------------------+
|          | [Obsidian]   | Markdown-based note-taking app with a bunch of fancy features. If      |
|          |              | Obsidian was open-source it would be top 3 software.                   |
+----------+--------------+------------------------------------------------------------------------+
| Music    | [Strawberry] | Cross-platform open-source Qt-based music player. The default icons    |
| player   |              | are a bit ugly but this can mostly be mitigated on Linux by turning on |
|          |              | "system icons". Strawberry fulfills my two requirements for a music    |
|          |              | player--Last.fm integration using the newer OAuth API (as opposed to   |
|          |              | plain-text username and password) and album-order preserving shuffle   |
|          |              | (e.g. in a playlist of mixed singles and albums, it will shuffle the   |
|          |              | playlist order but play albums in the original list order)--and is the |
|          |              | only Linux music player I've found that does so.                       |
|          +--------------+------------------------------------------------------------------------+
|          | [foobar2000] | Customizable freeware music player for Windows and macOS. The only     |
|          |              | other music player I know of that fulfills the two above requirements. |
|          |              | If foobar2000 was open-source it would be top 3 software.              |
+----------+--------------+------------------------------------------------------------------------+
| RSS      | [RSS Guard]  | Cross-platform open-source Qt-based RSS reader. Extremely featureful   |
| reader   |              | and looks really nice with a good Qt theme.                            |
+----------+--------------+------------------------------------------------------------------------+

<!-- prettier-ignore-end -->

[Neovim]: https://neovim.io/
[LazyVim]: https://www.lazyvim.org/
[Emacs]: https://www.gnu.org/software/emacs/
[Doom Emacs]: https://github.com/doomemacs/doomemacs
[VSCodium]: https://vscodium.com/
[Org mode]: https://orgmode.org/
[Obsidian]: https://obsidian.md/
[Strawberry]: https://www.strawberrymusicplayer.org/
[foobar2000]: https://www.foobar2000.org/
[RSS Guard]: https://github.com/martinrotter/rssguard

<!-- prettier-ignore-start -->

Table: Mobile applications

+-----------------------+--------------------------------------------------------------------------+
| App                   | Explanation                                                              |
+=======================+==========================================================================+
| [Aegis Authenticator] | Open-source multi-factor authenticator. Allows exports and setting a     |
|                       | vault password.                                                          |
+-----------------------+--------------------------------------------------------------------------+
| [Aves]                | Open-source photo gallery.                                               |
+-----------------------+--------------------------------------------------------------------------+
| [Signal]              | Private and secure open-source instant messenger. Often considered the   |
|                       | gold-standard of encrypted real-time communication.                      |
+-----------------------+--------------------------------------------------------------------------+
| [KDE Connect]         | Easily connect your desktop or laptop to your mobile devices. Very       |
|                       | useful for sharing notifications and files between devices.              |
+-----------------------+--------------------------------------------------------------------------+

<!-- prettier-ignore-end -->

[Aegis Authenticator]: https://getaegis.app/
[Aves]: https://github.com/deckerst/aves
[Signal]: https://signal.org/
[KDE Connect]: https://kdeconnect.kde.org/
