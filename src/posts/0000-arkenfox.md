<post-metadata>
  <post-title>Arkenfox installation guide</post-title>
  <post-date>2023-04-10</post-date>
  <update-date>2024-08-18</update-date>
  <post-tags>browsers, firefox, guide</post-tags>
  <post-license>cc-by-sa</post-license>
</post-metadata>

## Overview

<post-excerpt>[Arkenfox user.js](https://github.com/arkenfox/user.js) is a configuration file for desktop Firefox designed to increase the privacy and security of the browser while avoiding making the browser unusable.
This guide will walk you through how to install it and later explain some use cases and alternatives.</post-excerpt>
I recommend reading the [Arkenfox wiki](https://github.com/arkenfox/user.js/wiki/1.1-To-Arkenfox-or-Not) in order to decide whether to use Arkenfox first.

## Installing Firefox

If you already have Firefox installed, skip to [installing Arkenfox](#installing-arkenfox).

Otherwise, Firefox is available on these package managers with the following package names (non-exhaustive):

- APT: `firefox`
- DNF: `firefox`
- Flatpak: `org.mozilla.firefox`
- Homebrew: `firefox` (cask)
- Pacman: `firefox`
- Winget: `Mozilla.Firefox`

Alternatively, [download Firefox from Mozilla's site](https://www.mozilla.org/en-US/firefox/download/thanks/) or with your favorite graphical software center application.

## Installing Arkenfox

Type 'about:profiles' in the address bar to get to the profile management page.
Find the 'Root Directory' row of the table under your current profile and hit 'Open Directory'.
Alternatively, hit 'Create a New Profile', follow the profile creation wizard, hit 'Launch profile in new browser' under the new profile, and then do the same as above but with the new profile.

Then, open the [latest Arkenfox user.js release page](https://github.com/arkenfox/user.js/releases/latest) and download 'Source code (zip)'.
Extract the archive somewhere and open the resulting folder.
Copy the contents of that folder into the root folder you opened earlier.

In order to customize Arkenfox, we will use a file called 'user-overrides.js'.
You can write your own by following the [Arkenfox wiki page on user overrides](https://github.com/arkenfox/user.js/wiki/3.1-Overrides), or you can use one of the [user overrides presets I've made](https://codeberg.org/Liassica/user-overrides).

If you chose to use one of my user overrides, simply extract whichever archive you downloaded and place the 'user-overrides.js' file in the same folder as the other Arkenfox files.

Finally, exit Firefox (Ctrl+Shift+Q on Windows, Ctrl+Q on Linux) and run either `updater.bat` on Windows or `updater.sh` on Linux or MacOS.
Follow the on-screen prompts, updating the updater and running prefsCleaner when asked.
When you relaunch Firefox with your chosen profile, the Arkenfox user.js will be applied along with your overrides.

## Use cases and alternatives

Firefox with Arkenfox is best used when using [Tor Browser](https://www.torproject.org/download/) is impossible or impractical.
Firefox with Arkenfox will not give you as much protection as Tor Browser, but it's still far ahead of mainstream proprietary browsers like Chrome or Edge.
It can also be used alongside Tor Browser if one or the other doesn't meet all your needs.

Some alternatives to Firefox with Arkenfox include:

- [Librewolf](https://librewolf.net/), Firefox fork with more private defaults
- [Mullvad Browser](https://mullvad.net/en/browser), essentially Tor Browser but with [Mullvad VPN](https://mullvad.net/en/vpn) instead of [Tor](<https://en.wikipedia.org/wiki/Tor_(network)>)
- [Brave](https://brave.com/), [Chromium](https://www.chromium.org/Home/)-based

## Extensions

As explained on the [Arkenfox wiki page on extensions](https://github.com/arkenfox/user.js/wiki/4.1-Extensions), extensions have elevated privileges in your browser and can make you more vulnerable to fingerprinting and security flaws.
Therefore I recommend you stick mostly to the extensions listed on that Arkenfox page and to use as few as possible.

In addition to [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) as recommended by Arkenfox, I have also used an extension for my password manager, [Bitwarden](https://bitwarden.com/).

## Special treat for Nix users

Check out the wonderful [arkenfox-nixos](https://github.com/dwarfmaster/arkenfox-nixos) project for managing Arkenfox declaratively on NixOS or with Home Manager!
