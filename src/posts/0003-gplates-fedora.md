<post-metadata>
  <post-title>Compile GPlates on Fedora Linux</post-title>
  <post-date>2023-06-24</post-date>
  <update-date>2024-07-25</update-date>
  <post-tags>guide, linux, out of date</post-tags>
  <post-license>cc-by-sa</post-license>
</post-metadata>

## Overview

<post-excerpt>**Note: this guide is out of date. GPlates has a new stable version (2.4.0) and the download link for the old source code doesn't work anymore.** [GPlates](https://www.gplates.org/) is a free software program for modeling the tectonic history of a planet.
It was designed with a scientific audience in mind, but it has also gained popularity among the worldbuilding community.
GPlates has an Ubuntu package and both source- and binary-based Arch User Repository packages, but no package for Fedora Linux.
One of the options for running GPlates on Fedora is to compile GPlates from source; this guide will show you how to do so.</post-excerpt>
It was last confirmed to be working 2023-08-26.

## Download and dependencies

As of Fedora 38, compiling fails on the most recent stable version of GPlates,[^1] however the 2.3.1 beta 5 will compile as expected.[^2]
Download the [2.3.1 beta 5 source code](https://support.aarnet.edu.au/hc/en-us/sections/6547442372367) and extract the downloaded archive to wherever you like.

Opening the file `DEPS.Linux` will show you what dependencies are required to compile GPlates on Ubuntu.
Translating the packages to equivalent Fedora packages, we get the command:

`sudo dnf install cmake g++ mesa-libGL-devel mesa-libGLU-devel glew-devel python3-devel python3-numpy boost-devel qt5-qtbase-devel qt5-qtxmlpatterns-devel gdal-devel CGAL-devel proj-devel qwt-qt5-devel zlib-devel libXrender-devel libICE-devel libSM-devel freetype-devel fontconfig-devel`

Many of these are included by default in Fedora.

## Compiling and installing

Open the folder with the GPlates source code and open a terminal there.
Fedora puts Qwt in an odd spot, so you have to tell cmake where it is.[^3] Enter the command `cmake . -DQWT_INCLUDE_DIR=/usr/include/qt5/qwt`.
Once that has finished, run `make`. Compiling GPlates takes about 2 hours.

Once GPlates is done compiling, install it by running `sudo make install`.

## Running GPlates

GPlates does not yet support Wayland, so you must run it with the environment variable `QT_QPA_PLATFORM=xcb`.
This can be added to an alias or desktop entry.

![GPlates running on a Fedora 38 container in Fedora 38 Silverblue](/images/posts/gplates-fedora/gplates-gnome.webp)

![GPlates running on Fedora 38 KDE](/images/posts/gplates-fedora/gplates-kde.webp)

## References

[^1]: ["Compilation error with boost 1.77"](https://discourse.gplates.org/t/compilation-error-with-boost-1-77/452/1), _GPlates Community Forum_, 2021-12-20. Accessed 2023-06-24.

[^2]: [Comment on "Compilation error with boost 1.77"](https://discourse.gplates.org/t/compilation-error-with-boost-1-77/452/12), _GPlates Community Forum_, 2022-11-15. Accessed 2023-06-24.

[^3]: ["QWT_INCLUDE_DIR is not automatically found on Fedora 27"](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/-/issues/1663), _Orfeo Toolbox GitLab_, 2021-12-20. Accessed 2023-06-24.
