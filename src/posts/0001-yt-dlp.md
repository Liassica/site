<post-metadata>
  <post-title>Introduction to yt-dlp</post-title>
  <post-date>2023-04-22</post-date>
  <update-date>2024-01-03</update-date>
  <post-tags>command-line, guide, utility</post-tags>
  <post-license>cc-by-sa</post-license>
</post-metadata>

## Overview

<post-excerpt>[yt-dlp](https://github.com/yt-dlp/yt-dlp) is a command-line tool for downloading audio and video from YouTube and other sites.
This post will serve as a basic introduction to the program.</post-excerpt>

yt-dlp is a maintained fork of [youtube-dl](https://github.com/ytdl-org/youtube-dl) with many new features and improvements.
It allows extensive control over the output of the command, from format to quality to file name.

## Installing yt-dlp

yt-dlp is available from these package managers with the following package names (non-exhaustive):

- APT: `yt-dlp`
- DNF: `yt-dlp`
- Homebrew: `yt-dlp`
- Pacman: `yt-dlp`
- Pip: `yt-dlp`
- Winget: `yt-dlp`

If your preferred package is not listed or you wish to install yt-dlp manually, consult the [installation page of the yt-dlp wiki](https://github.com/yt-dlp/yt-dlp/wiki/Installation).

## Basic usage

To use yt-dlp, simply open a terminal in whichever folder you want to download to and type `yt-dlp <URL>`.

## FFmpeg

To access most of the more advanced features of yt-dlp, you will likely need to install [FFmpeg](https://ffmpeg.org/).
FFmpeg is a tool for processing audio and video and adds functionality to yt-dlp.
Like yt-dlp, FFmpeg is available from a variety of package managers (non-exhaustive):

- APT: `ffmpeg`
- DNF: `ffmpeg-free`
- Homebrew: `ffmpeg`
- Pacman: `ffmpeg`
- Winget: `Gyan.FFmpeg`

For other package managers, see the [Repology page for FFmpeg](https://repology.org/project/ffmpeg/versions).
Alternatively, [download FFmpeg manually from their site](https://ffmpeg.org/download.html).

## Options

yt-dlp has a vast selection of command options that allow fine-grained control over the output.
They can be found in the man page (`man yt-dlp`), help command (`yt-dlp -h` or `yt-dlp --help`), or on the [yt-dlp README](https://github.com/yt-dlp/yt-dlp#usage-and-options).

Some common yt-dlp options are:

- `-f` or `--format`: specify video format. For example, `yt-dlp -f mkv <URL>` will output the specified video in Matroska format. Also takes arguments like `bestvideo` and `bestaudio`.
- `-x` or `--extract-audio`: extract only the audio from the given video.
- `--audio-format`: specify the audio format. Useful in conjuction with `-x`. For example, `yt-dlp -x --audio-format opus <URL>` will output the given URL as Opus audio.
- `-o` or `--output`: specify the output template (file name). Supports a variety of field names (e.g. 'title', 'artist', 'uploader', etc.) and Python string formatting. Read the [output template section of the yt-dlp README](https://github.com/yt-dlp/yt-dlp#output-template) to get the most out of it.
