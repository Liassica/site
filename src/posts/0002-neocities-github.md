<post-metadata>
  <post-title>Deploy to Neocities through GitHub</post-title>
  <post-date>2023-05-06</post-date>
  <update-date>2024-07-28</update-date>
  <post-tags>git, guide</post-tags>
  <post-license>cc-by-sa</post-license>
</post-metadata>

## Overview

<post-excerpt>[Neocities](https://neocities.org/ "Neocities home page") is a free, adless webhosting service modeled after the defunct [GeoCities](https://en.wikipedia.org/wiki/GeoCities "GeoCities - Wikipedia").
However, Neocities' web editor is quite basic and there is no version control.
One way to alleviate these problems is to deploy your Neocities site through a GitHub repository.</post-excerpt>

This guide will assume you know how to use Git and GitHub.
If you don't, I recommend looking through [GitHub's "Get started with GitHub" documentation](https://docs.github.com/en/get-started "Get started with GitHub documentation - GitHub Docs").

## Setting up a GitHub repository

First, make a new GitHub repository.
Name it whatever you like, such as `site`, or `<GitHub username>.github.io` for easy mirroring on GitHub pages.
If you'd like to mirror to GitHub pages, read the section ["Mirroring on GitHub Pages"](#mirroring-on-github-pages) below along with the rest of this article.
Once it's created, clone it to a local repository.

Next, download your Neocities site.
You can do this from the Neocities dashboard by clicking 'Download entire site'.
![Download your Neocities site](/images/posts/neocities-github/download-site.webp "Download your Neocities site")

Extract the site archive into a subfolder of your repository.
Name the folder `public`.
The reason we do this is Neocities has a limited selection of allowed filetypes, and deploying straight from the root folder of your repository can break things.

After that, create a subfolder in the root of your repository called `.github`.
In the `.github` folder, create another folder called `workflows`.
Finally, download this <a href="/assets/posts/neocities-github/ci.yml" download="ci.yml">"Deploy to Neocities" workflow file</a> and put it in the `workflows` folder.

By this point, your repository should look something like this:
![Example repository directory structure](/images/posts/neocities-github/repo.webp "Example repository directory structure")

Next, you need to add your Neocities API key as a repository secret.
Go to Neocities settings > Manage Site Settings > API. This will show your API key.
On your GitHub repository, go to Settings > Secrets and variables > Actions > New repository secret.
Name the secret `NEOCITIES_API_KEY` and paste the key you got from Neocities as the value.
Be very careful with this API key, as it allows anyone or anything with access to it to change anything on your site.

Now all you need to do is push to your GitHub repository.
Any time you push to the main branch, it will deploy the site to Neocities.

## Mirroring on GitHub Pages

There are a few extra steps needed to be able to mirror your Neocities site on GitHub Pages.
First, make sure your repository is named `<GitHub username>.github.io`.
Then, make sure the folder containing your site is called `docs` and change the `dist_dir: public` line in the workflow file to `dist_dir: docs`.
This is because GitHub Pages (at least with a free account) can only be deployed from either the root folder or a `/docs` folder.

You may also want to tell GitHub to not ignore code statistics on the `/docs` folder like it does by default.
To do so, create a file called `.gitattributes` in your root directory and add the line `docs/* linguist-documentation=false` to it.

Finally, set up deploying to GitHub Pages from your GitHub repository by going to Settings > Pages.
Set the deploying branch to `main` and the deploying folder to `/docs`.
Now every time you push to your repository's main branch, it'll automatically build and deploy your GitHub Pages mirror.
