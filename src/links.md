# Links {.cc-by-nd}

Links to other sites

Table: Internet

| Site             | Description                                                     |
| ---------------- | --------------------------------------------------------------- |
| [Privacy Guides] | General recommendations for online security and privacy.        |
| [JustDeleteMe]   | Instructions on deleting nearly every online account.           |
| [Send]           | Easy encrypted file sharing.                                    |
| [Simple Icons]   | SVG logo icons for many popular brands, websites, and software. |

[Privacy Guides]: https://www.privacyguides.org/
[JustDeleteMe]: https://justdeleteme.xyz/
[Send]: https://send.vis.ee/
[Simple Icons]: https://simpleicons.org/

Table: Development

| Site              | Description                                                                                                                     |
| ----------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| [Can I use...]    | Easily determine which browsers support which web features.                                                                     |
| [MDN Web Docs]    | Extensive resources on everything related to web development.                                                                   |
| [kernelconfig.io] | Search for Linux kernel configuration options.                                                                                  |
| [LKDDB]           | Alternative to kernelconfig.io. Sometimes one site or the other won't have a given option, so having both to reference is nice. |

[Can I use...]: https://caniuse.com/
[MDN Web Docs]: https://developer.mozilla.org/docs/Web
[kernelconfig.io]: https://www.kernelconfig.io/
[LKDDB]: https://cateee.net/lkddb/web-lkddb/

Table: Games

| Site             | Description                                                                                        |
| ---------------- | -------------------------------------------------------------------------------------------------- |
| [Lichess]        | The better chess site. No features locked behind paywalls, many available without an account.      |
| [Modding OpenMW] | Modding resources for [OpenMW], an open source game engine for _The Elder Scrolls III: Morrowind_. |

[Lichess]: https://lichess.org/
[Modding OpenMW]: https://modding-openmw.com/
[OpenMW]: https://openmw.org/
